#include <stdio.h>
#include "siphash.h"
#include "halfsiphash.h"
#include "vectors.h"

int main(void)
{
	unsigned char * x = "WALRUSES LAUGH A LOT\n";

	unsigned char * k = "KEYS\n";

	unsigned char out[16];

	memset(out,0x00,sizeof(out)*sizeof(unsigned char));

	siphash(x,sizeof(x),k,out,sizeof(out));

	unsigned long long int i = 0;

	while ( i < sizeof(out) )
	{
		printf("%.2x ",out[i]);

		i++;
	}

	return 0;
}
