#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "siphash/siphash.h"
#include "siphash/halfsiphash.h"
#include "siphash/vectors.h"

#if 0
https://stackoverflow.com/questions/33482205/how-to-capture-the-output-of-execvp-function-in-c
#endif

#if 0
The actual shortened fingerprint is printed exactly

11 spaces after the "sig" string in the line

Remember, the line has to begin with "sig"

So you probably need to offset by 6 spaces

starting from the beginning of the "sig" substring

in the line. Then just jump past the rest

of the spaces until you hit the beginning

of the shortened fingerprint.

0. While scanning the sigs, check to ensure your

signature (by detecting your shortened fingerprint)

is present. If not, the script will prompt you

to sign the key.

0.4. For each key that must be signed, first

export the original version of the key as it

was to a /tmp/pub.key.asc file.

0.5. Then, delete the key from your keyring.

0.6. Then, import the key from the PGP keyserver.

0.7. Then, sign the key.

0.8.  Then, reupload the key to the PGP keyserver.

0.9. Then re-import the GPG key from /tmp/pub.key.asc

back to your GPG database.

1. So first export the --list-sigs output to

a string--do NOT put this in a file since

you need to do recursion.

2. Scan and find the first signature in the

list sigs list.

3. Check if the key is ***already*** present

using --list-keys. Again, store in string--not 

file.

3.5. If so, skip that key.

4. Else, download the key from keyserver.ubuntu.com

if you can.

5. Recursively invoke the find_key method

on the new key you just downloaded.

6. Once the recursive loop ends, resume scanning

the next line(s) in the --list-sigs output in

Step 1.

7. Finally, perform --check-sigs on the original

GPG key you started downloading from Step One.
#endif

typedef struct htable
{
	struct htable * link;

	unsigned char * shortened_fingerprint;

} htable;

int is_key_present
(
	unsigned char buf[]
	
	,
	
	unsigned long long int bufsize
	
	,
	
	const unsigned char * short_fingerprint

	,

	const unsigned long long int fingerprint_size
)
{
	memset(buf,0x00,bufsize*sizeof(unsigned char));

	strncat(buf,"gpg --list-keys ",16*sizeof(unsigned char));	

	strncat(buf,short_fingerprint,fingerprint_size);

	FILE * is_present = popen(buf,"r+");

	if ( is_present == 0x00 ) { fprintf(stderr,"is_present file reading failed\n"); exit(1); }
memset(buf,0x00,bufsize*sizeof(unsigned char));

	unsigned char * strstrp = 0x00;

	while
	(
	 	fgets(buf,bufsize,is_present)
		
		&&

		( ( strstrp = strstr(buf,short_fingerprint) ) == 0x00 )
	)
	{
		memset(buf,0x00,bufsize*sizeof(unsigned char));
	}

	if ( strstrp == 0x00 )
	{
		fclose(is_present);

		return 0;
	}
	
	fclose(is_present);

	return 1;
}	

void export_key
(
 	unsigned char buf[]
	
	,

	unsigned long long int bufsize

	,

	const unsigned char * short_fingerprint
	
	,
	
	const unsigned char * fingerprint_size

)
{
	memset(buf,0x00,bufsize*sizeof(unsigned char));
	
	strncat(buf,"gpg --export --armor ",bufsize);

	strncat(buf,short_fingerprint,fingerprint_size);

	strncat(buf," > ",4*sizeof(unsigned char));

	strncat(buf,short_fingerprint,fingerprint_size);

	strncat(buf,".pub.asc");

	FILE * export_key = 0x00;

	if
	(
		( export_key = popen(buf,"r+") ) == 0x00
	)
	{
		fprintf(stderr,"Failed to open export_key()\n");

		fclose(export_key);

		exit(1);
	}

	fclose(export_key);
	
}

void import_key
(
 	unsigned char buf[]
	
	,

	unsigned long long int bufsize

	,

	const unsigned char * short_fingerprint
	
	,
	
	const unsigned char * fingerprint_size

)
{
	memset(buf,0x00,bufsize*sizeof(unsigned char));

	strncat(buf,"gpg --import ",14*sizeof(unsigned char));

	strncat(buf,short_fingerprint,fingerprint_size*sizeof(unsigned char));

	strncat(buf,".pub.asc",9*sizeof(unsigned char));

	FILE * import_key = 0x00;

	if ( (import_key = popen(buf,"r+") ) == 0x00 )
	{
		fclose(import_key);

		fprintf(stderr,"Error: Failed to open import_key()\n");

		exit(1);
	}
	
	memset(buf,0x00,bufsize*sizeof(unsigned char));

	strncat(buf,"rm ",4*sizeof(unsigned char));

	strncat(buf,short_fingerprint,fingerprint_size);

	strncat(buf,".pub.asc",9*sizeof(unsigned char));

	if ( (import_key = popen(buf,"r+") ) == 0x00 )
	{
		fclose(import_key);

		fprintf(stderr,"Error: Failed to open import_key()\n");

		exit(1);
	}

	fclose(import_key);
}

#if 0
recv_key() must follow the same

protocol as download_keys.sh.

The protocol below is at the

time of this writing incorrect!
#endif

int recv_key
(
 	unsigned char buf[]
	
	,

	unsigned long long int bufsize

	,

	const unsigned char * short_fingerprint
	
	,
	
	const unsigned char * fingerprint_size

)
{
	memset(buf,0x00,bufsize*sizeof(unsigned char));

	strncat(buf,"curl https://keyserver.ubuntu.com/pks/lookup\?search\=0x",57*sizeof(unsigned char));

	strncat(buf,short_fingerprint,fingerprint_size*sizeof(unsigned char));

	strncat(buf,"\&fingerprint\=on\&op\=get | gpg --import",42*sizeof(unsigned char));

	FILE * recv_key = 0x00;
	
	if ( ( recv_key = popen(buf,"r+") ) == 0x00 )
	{
		fprintf(stderr,"Error: Failed to open recv_key()\n");

		fclose(recv_key);

		exit(1);
	}

	memset(buf,0x00,bufsize*sizeof(unsigned char));

	while ( fgets(buf,bufsize,recv_key) )
	{
		if	(
				strstr(buf,"Total number processed: 0") != 0x00
			)
			
		{
			fclose(recv_key);

			return 0;								
		}

		memset(buf,0x00,bufsize*sizeof(unsigned char));
	}

	fclose(recv_key);

	return 1;
}

#if 0
The sign_key() will list all

signatures and see if your

short fingerprint is listed

in a list of signatures.

Return 0 if target key specified

by short_fingerprint is already

signed by source key specified

by self_fingerprint.

Else, prompt user to sign

target key specified by

master_fingerprint.

After signing the key,

this function returns 1.
#endif

int sign_key
(
 	unsigned char buf[]
	
	,

	unsigned long long int bufsize

	,

	const unsigned char * self_fingerprint

	,

	const unsigned char * target_fingerprint
	
	,
	
	const unsigned char * fingerprint_size

)
{
	memset(buf,0x00,bufsize*sizeof(unsigned char));

	strncat(buf,"gpg --list-sigs ",17*sizeof(unsigned char));

	strncat(buf,target_fingerprint,fingerprint_size*sizeof(unsigned char));

	FILE * in = 0x00;

	if ( ( in = popen(buf,"r+") ) == 0x00 )
	{
		fprintf(stderr,"Error: Failed to list sigs in sign_key()\n");

		exit(1);
	}

	memset(buf,0x00,bufsize*sizeof(unsigned char));

	while ( fgets(buf,bufsize,in) )
	{
		if ( strstr(buf,self_fingerprint) != 0x00 )
		{
			return 0;
		}
	
		memset(buf,0x00,bufsize*sizeof(unsigned char));
	}

	memset(buf,0x00,bufsize*sizeof(unsigned char));

	strncat(buf,"gpg -u ",8*sizeof(unsigned char));

	strncat(buf,self_fingerprint,fingerprint_size*sizeof(unsigned char));

	strncat(buf," --sign-key ",13*sizeof(unsigned char));

	strncat(buf,target_fingerprint,fingerprint_size*sizeof(unsigned char));

	if ( ( in = popen(buf,"r+") ) == 0x00 )
	{
		fprintf(stderr,"Error: Failed to sign key...in sign_key()\n");

		exit(1);
	}

	return 1;
}

#if 0
This important function verifies

that your PGP key has all the

sigs that the master key has.

To do this, simply check that

all the short fingerprints

associated with each sig on

your key are found in the

master table and the number

of signatures equals the

number of signatures one the

master key.
#endif
void check_sigs
(
	sipout

	,

	sipout_size

	,

	const unsigned char * fingerprint
	
	,
	
	unsigned char buf[]
	
	,
	
	unsigned char line[]

	,
	
	unsigned char self_fingerprint[]

	,
	
	unsigned char short_fingerprint[]

	,

	htable * master_table[]

	,

	htable * table[]

	,

	const unsigned bufsize

	,

	const unsigned linesize

	,

	const unsigned fingerprint_size

	,

	const unsigned tablesize

)
{
	
}

void delete_key
(
	unsigned char buf[]
	
	,

	unsigned long long int bufsize

	,

	const unsigned char * short_fingerprint

	,

	const unsigned long long int fingerprint_size

)
{
	memset(buf,0x00,bufsize*sizeof(unsigned char));

	strncat(buf,"gpg --delete-key ",18*sizeof(unsigned char));

	strncat(buf,short_fingerprint,fingerprint_size*sizeof(unsigned char));

	FILE * deletekey = 0x00;

	if ( ( deletekey = popen(buf,"r+") ) == 0x00 )
	{
		fprintf(stderr,"Error: Failed to open deletekey\n");

		fclose(deletekey);

		exit(1);
	}

}

int find_entry
(
	unsigned char * short_fingerprint
	
	,
	
	unsigned short_fingerprint_size
	
	,
	
	htable * table[]
	
	,

	
	unsigned char sipout[]
	
	,
	
	unsigned long long int sipout_size
	
)
{

#if 0
	Capture the last 11 bits of the

	128 bit output
#endif

	memset(out,0x00,16*sizeof(unsigned char));	

	memset(sipout,0x00,sipout_size*sizeof(unsigned char));
	
	siphash(short_fingerprint,short_fingerprint_size,"Key",&entry[0],sipout,sipout_size);

	unsigned short entry = 0;

	entry |= sipout[sipout_size-1];

	// so the hashtable is 2048 buckets large.
	//
	// That is 2^11 buckets large.

	entry |= ( ( sipout[sipout_size-2] & 0x07 ) << 8 );

	htable ** k = table;

	while
	(
		(
			(*k) != 0x00	
		)

		&&

		(
			strncmp((*k)->shortened_fingerprint,shortened_fingerprint,fingerprint_size) != 0
		)
	)
	{
		k = &( (*k)->link );
	}


	if 	(
		  	(	
				(*k) != 0x00
			)

			&&
		
			strncmp((*k)->shortened_fingerprint,shortened_fingerprint,fingerprint_size) == 0
		
		)
	{
		return 1;
	}

		
	return 0;
}

void add_entry
(

	unsigned char * short_fingerprint
	
	,
	
	unsigned short_fingerprint_size
	
	,
	
	htable * table[]
	
	,
	
	unsigned char sipout[]
	
	,
	
	unsigned long long int sipout_size
	

)
{

#if 0
	Capture the last 11 bits of the

	128 bit output
#endif

	memset(out,0x00,16*sizeof(unsigned char));	

	memset(sipout,0x00,sipout_size*sizeof(unsigned char));
	
	siphash(short_fingerprint,short_fingerprint_size,"Key",&entry[0],sipout,sipout_size);

	unsigned short entry = 0;

	entry |= sipout[sipout_size-1];

	entry |= ( ( sipout[sipout_size-2] & 0x07 ) << 8 );

	htable ** k = table;

	while
	(
		(
			(*k) != 0x00	
		)

		&&

		(
			strncmp((*k)->shortened_fingerprint,shortened_fingerprint,fingerprint_size) != 0
		)
	)
	{
		k = &( (*k)->link );
	}

	if ( (*k) == 0x00 )
	{
		(*k) = calloc(1,sizeof(htable));

		memcpy((*k)->shortened_fingerprint,shortened_fingerprint,fingerprint_size);

		return 0;
	}

	// ( strncmp((*k)->shortened_fingerprint,shortened_fingerprint,fingerprint_size) == 0 )
	
	return entry;
}

#if 0
fill_master_table must

return the total number

of signatures in the

PGP master key.
#endif
unsigned long long int fill_master_table
(
	sipout

	,

	sipout_size

	,

	const unsigned char * fingerprint
	
	,
	
	unsigned char buf[]
	
	,
	
	unsigned char line[]

	,
	
	unsigned char self_fingerprint[]

	,
	
	unsigned char short_fingerprint[]

	,


	htable * master_table[]

	,

	const unsigned bufsize

	,

	const unsigned linesize

	,

	const unsigned fingerprint_size

	,

	const unsigned tablesize

)
{
	memset(line,0x00,linesize*sizeof(unsigned char));

	memset(buf,0x00,bufsize*sizeof(unsigned char));

	memset(short_fingerprint,0x00,fingerprint_size*sizeof(unsigned char));
	
	strncat(buf,"gpg --list-sigs ",bufsize);
	
	strncat(buf,fingerprint,bufsize);
	
	FILE * in = popen(buf,"r");

	if ( in == 0x00 ) exit(1);

	unsigned long long int sigcount = 0;

	const unsigned sig_offset = 13;
	
	unsigned char * bufp = &buf[0];

	unsigned char * linep = &line[0];

	unsigned char * short_fingerprint_p = &short_fingerprint[0];

	int key_present_status = 0;

	
	while ( fgets(line,linesize*sizeof(unsigned char),in) )
	{
	//	printf("%s\n",line);

		linep = &line[0];

		short_fingerprint_p = &short_fingerprint[0];

		if (
			(line[0] == 's')

			&&

			(line[1] == 'i')
			
			&&

			(line[2] == 'g')

		   )
	
		{
			linep += sig_offset;

			while ( *linep == ' ' )
			{
				linep++;	
			}

	//		printf("After spaces:%s\n",linep);
		
			memset(short_fingerprint,0x00,fingerprint_size*sizeof(unsigned char));

			while ( *linep != 0x20 )
			{
				*short_fingerprint_p++ = *linep++;

			}

			*short_fingerprint_p = 0x00;

	//		printf("%s\n",short_fingerprint);
			
			sigcount++;

		}

		else	// no sig in the current line
		{
			continue;
		}

		if	(
				(

					find_entry(short_fingerprint,fingerprint_size,master_table,sipout,sipout_size)

					==

					0

				)

				&&

				// ensure current sig short fingerprint does not match
				//
				// source short fingerprint
				
				( strncmp(fingerprint,short_fingerprint,fingerprint_size) != 0 )				
			)
		{

			add_entry(short_fingerprint,fingerprint_size,master_table,sipout,sipout_size);

			// Now we need to download all keys that signed
			//
			// the PGP key fingerprinted as short_fingerprint 
		
		}

		memset(line,0x00,linesize*sizeof(unsigned char));

		memset(short_fingerprint,0x00,fingerprint_size*sizeof(unsigned char));

	}

	return sigcount;
	
	fclose(in);

}

void clear_table(htable * table[],unsigned long long int tablesize)
{
	htable ** k = 0x00;

	for ( unsigned long long int i = 0, j = tablesize-1 ; i < j; i++, j-- )
	{
		k = &table[i];

		while ( (*k) != 0x00 )
		{
			k = &( (*k)->link );	
		}

		k = &table[j];

		while ( (*k) != 0x00 )
		{
			k = &( (*k)->link );	
		}

	}	
}

#if 0
download_keys first downloads

master PGP key then all PGP keys

that signed the master key.

Having a master key avoids having

to do a graph traversal to find

all keys in a web of trust.
#endif
int download_keys
(
	sipout

	,

	sipout_size

	,

	const unsigned char * fingerprint
	
	,
	
	unsigned char buf[]
	
	,
	
	unsigned char line[]

	,
	
	unsigned char self_fingerprint[]

	,
	
	unsigned char short_fingerprint[]

	,

	htable * master_table[]

	,

	htable * table[]

	,

	const unsigned bufsize

	,

	const unsigned linesize

	,

	const unsigned fingerprint_size

	,

	const unsigned tablesize

)
{
	memset(line,0x00,linesize*sizeof(unsigned char));

	memset(buf,0x00,bufsize*sizeof(unsigned char));

	memset(short_fingerprint,0x00,fingerprint_size*sizeof(unsigned char));
	
	strncat(buf,"gpg --list-sigs ",bufsize);
	
	strncat(buf,fingerprint,bufsize);
	
	FILE * in = popen(buf,"r");

	if ( in == 0x00 ) exit(1);

	const unsigned sig_offset = 13;
	
	unsigned char * bufp = &buf[0];

	unsigned char * linep = &line[0];

	unsigned char * short_fingerprint_p = &short_fingerprint[0];

	int key_present_status = 0;
	
	while ( fgets(line,linesize*sizeof(unsigned char),in) )
	{
	//	printf("%s\n",line);

		linep = &line[0];

		short_fingerprint_p = &short_fingerprint[0];

		if (
			(line[0] == 's')

			&&

			(line[1] == 'i')
			
			&&

			(line[2] == 'g')

		   )
	
		{
			linep += sig_offset;

			while ( *linep == ' ' )
			{
				linep++;	
			}

	//		printf("After spaces:%s\n",linep);
		
			memset(short_fingerprint,0x00,fingerprint_size*sizeof(unsigned char));

			while ( *linep != 0x20 )
			{
				*short_fingerprint_p++ = *linep++;

			}

			*short_fingerprint_p = 0x00;

	//		printf("%s\n",short_fingerprint);

		}

		else	// no sig in the current line
		{
			continue;
		}

		if	(
				(

					find_entry(short_fingerprint,fingerprint_size,master_table,sipout,sipout_size)

					==

					1

				)

				&&

				(

					find_entry(short_fingerprint,fingerprint_size,table,sipout,sipout_size)

					==

					0

				)

				&&

				// ensure current sig short fingerprint does not match
				//
				// source short fingerprint
				
				( strncmp(short_fingerprint,self_fingerprint,fingerprint_size) != 0 )			
			)
		{

			recv_key(buf,bufsize,short_fingerprint,fingerprint_size);
			
			add_entry(short_fingerprint,fingerprint_size,table,sipout,sipout_size);

		}

		memset(line,0x00,linesize*sizeof(unsigned char));

		memset(short_fingerprint,0x00,fingerprint_size*sizeof(unsigned char));

	}
	
	fclose(in);

	return 0;	
}

#if 0
The first argument is the GPG fingerprint

you want to use to sign keys with.

The second argument is the GPG fingerprint

of the first PGP key you want to download.
#endif

int main(int argc,char*argv[])
{

#if 0
	FILE * in = popen("ls -la","r");

	unsigned char buf[1025];

	memset(buf,0x00,1024*sizeof(unsigned char));

	while ( fgets(buf,sizeof(buf),in) )
	{
		printf("%s",buf);

		buf[1024] = 0x00;
	}
#endif
	const unsigned bufsize = 256;

	const unsigned linesize = 1024;

	const unsigned fingerprint_size = 48;
	
	const unsigned tablesize = 2048;

	unsigned char buf[bufsize+1];

	unsigned char line[linesize+1];

	htable * master_table[tablesize+1];
	
	htable * table[tablesize+1];

	unsigned char short_fingerprint[fingerprint_size+1];

	unsigned char self_fingerprint[fingerprint_size+1];

	memset(table,0x00,(tablesize+1)*sizeof(htable*));

	memset(buf,0x00,(bufsize+1)*sizeof(unsigned char));

	memset(line,0x00,(linesize+1)*sizeof(unsigned char));

	memset(short_fingerprint,0x00,(fingerprint_size+1)*sizeof(unsigned char));
	
	memset(self_fingerprint,0x00,(fingerprint_size+1)*sizeof(unsigned char));
	
	unsigned char sipout[16];

	memset(sipout,0x00,16*sizeof(unsigned char));	

	download_keys(sipout,16,argv[1],buf,line,self_fingerprint,short_fingerprint,master_table,table,bufsize,linesize,fingerprint_size,tablesize);

	return 0;

}
