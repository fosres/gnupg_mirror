#!/bin/bash

set -o xtrace

git add -A

export_GPG_TTY=$(tty)

set +o xtrace

echo "Enter your git commit message below:"

read COMMIT_MESSAGE

echo "Enter the branch you want to make a"

echo "push request below:"

read BRANCH

set -o xtrace

git branch

git commit -S -m "$COMMIT_MESSAGE"

git push origin $BRANCH
