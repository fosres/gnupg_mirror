#!/bin/bash

'''
https://stackoverflow.com/questions/67251078/gpg-keyserver-send-failed-no-keyserver-available-when-sending-to-hkp-pool
'''
echo "Enter the full GPG fingerprint"

echo ""

echo "of the GPG key you wish to"

echo ""

echo "download:"

read FINGERPRINT

echo $FINGERPRINT

FIRST_URL_PART=https://keyserver.ubuntu.com/pks/lookup?search\=0x

SECOND_URL_PART=\&fingerprint\=on\&op\=get

WHOLE_URL=$FIRST_URL_PART$FINGERPRINT$SECOND_URL_PART

echo $WHOLE_URL

set -o xtrace

curl $WHOLE_URL > /tmp/$FINGERPRINT.pub.asc

gpg --import /tmp/$FINGERPRINT.pub.asc

rm /tmp/$FINGERPRINT.pub.asc
