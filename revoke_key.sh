#!/bin/bash

set -o xtrace

gpg --list-keys

set +o xtrace

echo "Enter your full GPG fingerprint"

echo ""

echo "that you want to revoke:"

read FINGERPRINT

set -o xtrace

gpg --output $FINGERPRINT.revoke.asc --gen-revoke $FINGERPRINT

gpg --import $FINGERPRINT.revoke.asc

gpg --keyserver keyserver.ubuntu.com --send-key $FINGERPRINT

gpg --delete-secret-and-public-key $FINGERPRINT

rm $FINGERPRINT.revoke.asc

