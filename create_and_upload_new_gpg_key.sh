#!/bin/bash

set -o xtrace

#Add uid with the "....." as "Real Name:" in the BELOW command:

gpg --expert --full-generate-key

gpg --list-keys > /tmp/new_pubring.asc

gcc find_fingerprint.c -o /tmp/find_fingerprint.o

FINGERPRINT=$(/tmp/find_fingerprint.o)

rm /tmp/new_pubring.asc

rm /tmp/find_fingerprint.o

# Remember to add BLANK uid 

# Then delete "....." uid instantly

gpg --edit-key $FINGERPRINT

gpg --keyserver keyserver.ubuntu.com --send-keys $FINGERPRINT
