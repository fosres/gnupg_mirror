#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void reverse(unsigned char * s,unsigned long long int len)
{
	unsigned char temp = 0;

	for ( unsigned long long int i = 0, j = len - 1 ; i < j ; i++, j-- )
	{
		temp = s[i];

		s[i] = s[j];

		s[j] = temp;	
	}
}
unsigned char * find_key(unsigned char * ans)
{
	unsigned char buf[256];

	unsigned char * ans_p = &ans[0];

	memset(ans,0x00,256*sizeof(unsigned char));

	memset(buf,0x00,256*sizeof(unsigned char));

	FILE * in = fopen("/tmp/new_pubring.asc","rw+");

	if ( in == 0x00 )
	{
		fprintf(stderr,"Failed to open and read /tmp/new_pubring.asc\n");

		exit(1);
	}

	fseek(in,0x00,SEEK_END);

	long offset = ftell(in);
	
	offset -= 4;

	fseek(in,offset,SEEK_END);

	fread(buf,sizeof(unsigned char),4,in);

	while ( strstr(buf,"\nuid") == NULL )
	{
		offset--;

		fseek(in,offset,SEEK_END);

		fread(buf,sizeof(unsigned char),4,in);

	}

	offset--;

	fseek(in,offset,SEEK_END);
	
	memset(buf,0x00,1*sizeof(unsigned char));

	fread(buf,sizeof(unsigned char),1,in);

	while ( buf[0] != 0x20 )
	{
		*ans_p++ = buf[0];

		offset--;

		fseek(in,offset,SEEK_END);
		
		memset(buf,0x00,1*sizeof(unsigned char));

		fread(buf,sizeof(unsigned char),1,in);
	}

	reverse(ans,strnlen(ans,256*sizeof(unsigned char)));

	return ans;

}

int main(void)
{
	unsigned char * ans = (unsigned char*)calloc(256,sizeof(unsigned char));

	printf("%s\n",find_key(ans));

	return 0;
}
