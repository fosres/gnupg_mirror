#!/bin/bash

: '
The actual shortened fingerprint is printed exactly

11 spaces after the \"sig\" string in the line

Remember, the line has to begin with \"sig\"

So you probably need to offset by 6 spaces

starting from the beginning of the \"sig\" substring

in the line. Then just jump past the rest

of the spaces until you hit the beginning

of the shortened fingerprint.

0. While scanning the sigs, check to ensure your

signature (by detecting your shortened fingerprint)

is present. If not, the script will prompt you

to sign the key.

0.4. For each key that must be signed, first

export the original version of the key as it

was to a /tmp/pub.key.asc file.

0.5. Then, delete the key from your keyring.

0.6. Then, import the key from the PGP keyserver.

0.7. Then, sign the key.

0.8.  Then, reupload the key to the PGP keyserver.

0.9. Then re-import the GPG key from /tmp/pub.key.asc

back to your GPG database.

1. So first export the --list-sigs output to

a string--do NOT put this in a file since

you need to do recursion.

2. Scan and find the first signature in the

list sigs list.

3. Check if the key is ***already*** present

using --list-keys. Again, store in string--not 

file.

3.5. If so, skip that key.

4. Else, download the key from keyserver.ubuntu.com

if you can.

5. Recursively invoke the find_key method

on the new key you just downloaded.

6. Once the recursive loop ends, resume scanning

the next line(s) in the --list-sigs output in

Step 1.

7. Finally, perform --check-sigs on the original

GPG key you started downloading from Step One.

'
set +o xtrace

echo "Enter the full GPG fingerprint"

echo ""

echo "of the GPG key you wish to"

echo ""

echo "download:"

read FINGERPRINT

echo $FINGERPRINT

FIRST_URL_PART=https://keyserver.ubuntu.com/pks/lookup?search\=0x

SECOND_URL_PART=\&fingerprint\=on\&op\=get

WHOLE_URL=$FIRST_URL_PART$FINGERPRINT$SECOND_URL_PART

echo $WHOLE_URL

echo "Enter your GPG full fingerprint:"

read SIGNING_FINGERPRINT

set -o xtrace


#Perform all of the commands in find_key.c

#for better parsing of output

'''

gpg --export --armor $FINGERPRINT > /tmp/$FINGERPRINT.pre.pub.asc

gpg --delete-key $FINGERPRINT

#Add a test to check if something was actually downloaded

curl $WHOLE_URL > /tmp/$FINGERPRINT.server.pub.asc

gpg --import /tmp/$FINGERPRINT.server.pub.asc

'''

gcc find_key.c siphash/*.c siphash/*.h -o find_key.o
