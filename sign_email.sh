#!/bin/bash

gpg --list-secret-keys > /tmp/new_secring.asc

gcc find_secret_fingerprint.c -o /tmp/find_secret_fingerprint.o

FINGERPRINT=$(/tmp/find_secret_fingerprint.o)

rm /tmp/new_secring.asc

rm /tmp/find_secret_fingerprint.o


echo "Copy and paste the output to your email message panel."

echo ""

echo "Press EOF key (Ctrl+D for Linux)"

echo ""

echo "or (Ctrl+Z for Windows)"

echo ""

echo "when done with writing file:"

set -o xtrace

gpg -u $FINGERPRINT --clearsign --armor 
