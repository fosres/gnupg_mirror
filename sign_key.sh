#!/bin/bash

set -o xtrace

gpg --list-keys

set +o xtrace

echo "Enter your full GPG fingerprint: "

read SIGNING_FINGERPRINT

echo ""

echo "Enter the full GPG fingerprint"

echo ""

echo "you wish to sign: "

read SIGNED_FINGERPRINT

set -o xtrace

gpg --export --armor $SIGNED_FINGERPRINT > $SIGNED_FINGERPRINT.pub.asc

gpg --delete-key $SIGNED_FINGERPRINT

gpg --keyserver keyserver.ubuntu.com --recv-key $SIGNED_FINGERPRINT

gpg -u $SIGNING_FINGERPRINT --sign-key $SIGNED_FINGERPRINT

gpg --keyserver keyserver.ubuntu.com --send-key $SIGNED_FINGERPRINT

gpg --import $SIGNED_FINGERPRINT.pub.asc

rm $SIGNED_FINGERPRINT.pub.asc
